//============================================================================
//  Gameboy
//  Copyright (c) 2015 Till Harbaum <till@harbaum.org>
//
//  Port to MiSTer
//  Copyright (C) 2017,2018 Sorgelig
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

module emu
(
	//Master input clock
	input         CLK_50M,

	//Async reset from top-level module.
	//Can be used as initial reset.
	input         RESET,

	//Must be passed to hps_io module
	inout  [45:0] HPS_BUS,

	//Base video clock. Usually equals to CLK_SYS.
	output        CLK_VIDEO,

	//Multiple resolutions are supported using different CE_PIXEL rates.
	//Must be based on CLK_VIDEO
	output        CE_PIXEL,

	//Video aspect ratio for HDMI. Most retro systems have ratio 4:3.
	output  [7:0] VIDEO_ARX,
	output  [7:0] VIDEO_ARY,

	output  [7:0] VGA_R,
	output  [7:0] VGA_G,
	output  [7:0] VGA_B,
	output        VGA_HS,
	output        VGA_VS,
	output        VGA_DE,    // = ~(VBlank | HBlank)
	output        VGA_F1,
	output  [1:0] VGA_SL,

	output        LED_USER,  // 1 - ON, 0 - OFF.

	// b[1]: 0 - LED status is system status OR'd with b[0]
	//       1 - LED status is controled solely by b[0]
	// hint: supply 2'b00 to let the system control the LED.
	output  [1:0] LED_POWER,
	output  [1:0] LED_DISK,

	output [15:0] AUDIO_L,
	output [15:0] AUDIO_R,
	output        AUDIO_S,   // 1 - signed audio samples, 0 - unsigned
	output  [1:0] AUDIO_MIX, // 0 - no mix, 1 - 25%, 2 - 50%, 3 - 100% (mono)

	//ADC
	inout   [3:0] ADC_BUS,

	// SD-SPI
	output        SD_SCK,
	output        SD_MOSI,
	input         SD_MISO,
	output        SD_CS,
	input         SD_CD,

	//High latency DDR3 RAM interface
	//Use for non-critical time purposes
	output        DDRAM_CLK,
	input         DDRAM_BUSY,
	output  [7:0] DDRAM_BURSTCNT,
	output [28:0] DDRAM_ADDR,
	input  [63:0] DDRAM_DOUT,
	input         DDRAM_DOUT_READY,
	output        DDRAM_RD,
	output [63:0] DDRAM_DIN,
	output  [7:0] DDRAM_BE,
	output        DDRAM_WE,

	//SDRAM interface with lower latency
	output        SDRAM_CLK,
	output        SDRAM_CKE,
	output [12:0] SDRAM_A,
	output  [1:0] SDRAM_BA,
	inout  [15:0] SDRAM_DQ,
	output        SDRAM_DQML,
	output        SDRAM_DQMH,
	output        SDRAM_nCS,
	output        SDRAM_nCAS,
	output        SDRAM_nRAS,
	output        SDRAM_nWE,

	input         UART_CTS,
	output        UART_RTS,
	input         UART_RXD,
	output        UART_TXD,
	output        UART_DTR,
	input         UART_DSR,

	// Open-drain User port.
	// 0 - D+/RX
	// 1 - D-/TX
	// 2..5 - USR1..USR4
	// Set USER_OUT to 1 to read from USER_IN.
	input   [5:0] USER_IN,
	output  [5:0] USER_OUT,

	input         OSD_STATUS
);

assign ADC_BUS  = 'Z;
assign USER_OUT = '1;
assign {DDRAM_CLK, DDRAM_BURSTCNT, DDRAM_ADDR, DDRAM_DIN, DDRAM_BE, DDRAM_RD, DDRAM_WE} = 0;
assign VGA_F1 = 0;

assign {UART_RTS, UART_TXD, UART_DTR} = 0;

assign {SD_SCK, SD_MOSI, SD_CS} = 'Z;

assign LED_USER  = ioctl_download | (!sav_pending);
assign LED_DISK  = 0;
assign LED_POWER = 0;

assign VIDEO_ARX = status[4:3] == 2'b10 ? 8'd16:
						status[4:3] == 2'b01 ? 8'd10:
						8'd4;

assign VIDEO_ARY = status[4:3] == 2'b10 ? 8'd9:
						status[4:3] == 2'b01 ? 8'd9:
						8'd3;

assign AUDIO_MIX = status[8:7];
assign AUDIO_S = 0;

`define DUALCORE

`ifdef DUALCORE
localparam NUM_CORES = 2;
`else
localparam NUM_CORES = 1;
`endif

`include "build_id.v"
localparam CONF_STR1 = {
	"GAMEBOY;;",
	"-;",
	"FS,GBCGB ,Load ROM;",
	"OEF,System,Auto,Gameboy,Gameboy Color;",
	"-;",
	"C,Cheats;"
};

localparam CONF_STR2 = {
	"H,Cheats enabled,Yes,No;",
	"-;",
	"OC,Inverted color,No,Yes;",
	"O1,Palette,Grayscale,Custom;"
};

localparam CONF_STR3 = {
	",GBP,Load Palette;",
	"-;",
	"OD,OSD triggered autosaves,No,Yes;"
};

localparam CONF_STR4 = {
	"9,Load Backup RAM;"
};

localparam CONF_STR5 = {
	"A,Save Backup RAM;",
	"-;",
	"O34,Aspect ratio,4:3,10:9,16:9;",
	"O78,Stereo mix,none,25%,50%,100%;",
	"OGH,Linked audio,GB 1,GB 2,Split L/R,Mix;",
	"-;",
	"O2,Boot,Normal,Fast;",
	"-;",
	"R0,Reset;",
	"J1,A,B,Select,Start;",
	"V,v",`BUILD_DATE
};

////////////////////   CLOCKS   ///////////////////

wire clk_sys;
wire pll_locked;

pll pll
(
	.refclk(CLK_50M),
	.rst(0),
	.outclk_0(clk_sys),
	.outclk_1(SDRAM_CLK),
	.locked(pll_locked)
);

///////////////////////////////////////////////////

wire [31:0] status;
wire  [1:0] buttons;

wire        ioctl_download;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;
reg         ioctl_wait;

wire [15:0] joystick[2];
wire [7:0]  filetype;

reg  [31:0] sd_lba[NUM_CORES];
reg         sd_rd[NUM_CORES];
reg         sd_wr[NUM_CORES];
wire        sd_ack;
wire  [8:0] sd_buff_addr;
wire  [7:0] sd_buff_dout;
wire  [7:0] sd_buff_din[NUM_CORES];
wire        sd_buff_wr;
wire        img_mounted;
wire        img_readonly;
wire [63:0] img_size;



hps_io #(.STRLEN(($size(CONF_STR1)>>3) + ($size(CONF_STR2)>>3) + ($size(CONF_STR3)>>3) + ($size(CONF_STR4)>>3) + ($size(CONF_STR5)>>3) + 4)) hps_io
(
	.clk_sys(clk_sys),
	.HPS_BUS(HPS_BUS),

	.conf_str({CONF_STR1, gg_available[0] ? "O" : "+",CONF_STR2, status[1]?"F":"+",CONF_STR3, sav_char, CONF_STR4, sav_char, CONF_STR5}),

	.ioctl_download(ioctl_download),
	.ioctl_wr(ioctl_wr),
	.ioctl_addr(ioctl_addr),
	.ioctl_dout(ioctl_dout),
	.ioctl_wait(ioctl_wait),
	.ioctl_index(filetype),

	.sd_lba(sd_lba[0]),
	.sd_rd(sd_rd[0]),
	.sd_wr(sd_wr[0]),
	.sd_ack(sd_ack),
	.sd_buff_addr(sd_buff_addr),
	.sd_buff_dout(sd_buff_dout),
	.sd_buff_din(sd_buff_din[0]),
	.sd_buff_wr(sd_buff_wr),
	.img_mounted(img_mounted),
	.img_readonly(img_readonly),
	.img_size(img_size),

	.buttons(buttons),
	.status(status),

	.joystick_0(joystick[0]),
	.joystick_1(joystick[1])
);

wire [7:0] sav_char = sav_supported[0] ? "R" : "+";

///////////////////////////////////////////////////

wire cart_download    = ioctl_download && (filetype == 8'h01 || filetype == 8'h41 || filetype == 8'h80);
wire palette_download = ioctl_download && (filetype == 8'h07 || filetype == 8'h06 || filetype == 8'h00);
wire bios_download    = ioctl_download && (filetype == 8'h40);
wire code_download    = &filetype;

wire [15:0] cart_addr[NUM_CORES];
wire [7:0]  cart_do[NUM_CORES];
wire [7:0]  cart_di[NUM_CORES];    // data from cpu to cart
wire cart_rd[NUM_CORES];
wire cart_wr[NUM_CORES];

wire [9:0] cart_mbc_bank[NUM_CORES];
wire [7:0] cart_ram_size[NUM_CORES];
wire sav_supported[NUM_CORES];
wire [NUM_CORES-1:0] sav_pending;

wire isGBC_game[NUM_CORES];

wire [11:0] bios_addr[NUM_CORES];
wire  [7:0] bios_do[NUM_CORES];

wire [128:0] gg_code[NUM_CORES];
wire         gg_available[NUM_CORES];

wire serial_clk_out[NUM_CORES];
wire serial_data_out[NUM_CORES];

wire [7:0] sdram_do[NUM_CORES];
wire [7:0] sdram_di[NUM_CORES];
wire [24:0] sdram_addr[NUM_CORES];
wire sdram_oe[NUM_CORES];
wire sdram_we[NUM_CORES];

sdram sdram (
	// interface to the MT48LC16M16 chip
	.SDRAM_DQ   ( SDRAM_DQ   ),
	.SDRAM_A    ( SDRAM_A    ),
	.SDRAM_DQML ( SDRAM_DQML ),
	.SDRAM_DQMH ( SDRAM_DQMH ),
	.SDRAM_BA   ( SDRAM_BA   ),
	.SDRAM_nCS  ( SDRAM_nCS  ),
	.SDRAM_nWE  ( SDRAM_nWE  ),
	.SDRAM_nRAS ( SDRAM_nRAS ),
	.SDRAM_nCAS ( SDRAM_nCAS ),
	.SDRAM_CKE  ( SDRAM_CKE  ),

	// system interface
	.clk            ( clk_sys                   ),
	.init           ( ~pll_locked               ),

	// cpu interface
	.ch0_addr   ( sdram_addr[0]   ),
	.ch0_wr     ( sdram_we[0]     ),
	.ch0_din    ( sdram_di[0]     ),
	.ch0_rd     ( sdram_oe[0]     ),
	.ch0_dout   ( sdram_do[0]     ),

`ifndef DUALCORE
	.ch1_addr   (  ),
	.ch1_wr     (  ),
	.ch1_din    (  ),
	.ch1_rd     (  ),
	.ch1_dout   (  ),
`else
	.ch1_addr   ( sdram_addr[1]   ),
	.ch1_wr     ( sdram_we[1]     ),
	.ch1_din    ( sdram_di[1]     ),
	.ch1_rd     ( sdram_oe[1]     ),
	.ch1_dout   ( sdram_do[1]     ),
`endif
);

reg cart_ready = 0;
reg dn_write;
always @(posedge clk_sys) begin
	if(ioctl_wr) ioctl_wait <= 1;

	if(speed[0]?ce_cpu2x:ce_cpu) begin
		dn_write <= ioctl_wait;
		if(dn_write) {ioctl_wait, dn_write} <= 0;
		if(dn_write) cart_ready <= 1;
	end
end

reg [127:0] palette = 128'h828214517356305A5F1A3B4900000000;

always @(posedge clk_sys) begin
	if (palette_download & ioctl_wr) begin
			palette[127:0] <= {palette[119:0], ioctl_dout};
	end
end

wire reset = (RESET | status[0] | buttons[1] | cart_download); // | bk_loading);

wire lcd_clkena[NUM_CORES];
wire [14:0] lcd_data[NUM_CORES];
wire [1:0] lcd_mode[NUM_CORES];
wire lcd_on[NUM_CORES];

wire speed[NUM_CORES];

wire [15:0] audio_l[NUM_CORES], audio_r[NUM_CORES];


reg isGBC = 0;
always @(posedge clk_sys) if(reset) isGBC <= status[15:14] ? status[15] : !filetype[7:4];

generate
genvar i;
for(i=0;i<NUM_CORES;i=i+1) begin : spr
	assign sdram_di[i]   = cart_download ? ioctl_dout : 8'd0;
	assign sdram_addr[i] = cart_download ?
									ioctl_addr :
									{2'b00, cart_mbc_bank[i], cart_addr[i][12:0]};
	assign sdram_oe[i]   = ~cart_download & cart_rd[i]; // & ~cram_rd;
	assign sdram_we[i]   =  cart_download & dn_write;

	cart gb_cart_if(
		.reset( reset ),
		.clk_sys( clk_sys ),
		.clk_cpu2x( clk_cpu2x ),

		.ce_cpu2x( ce_cpu2x ),
		.cart_ready( cart_ready ),

		.init( ~pll_locked ),

		.cart_download( cart_download ),
		.bios_download( bios_download ),
		.code_download( code_download ),

		.sdram_do ( sdram_do[i] ),

		.cart_addr( cart_addr[i] ),
		.cart_rd( cart_rd[i] ),
		.cart_wr( cart_wr[i] ),
		.cart_di( cart_di[i] ),
		.cart_do( cart_do[i] ),

		.mbc_bank ( cart_mbc_bank[i] ),

		.cart_ram_size( cart_ram_size[i] ),
		.isGBC_game( isGBC_game[i] ),

		.bios_addr( bios_addr[i] ),
		.bios_do( bios_do[i] ),

		.gg_code( gg_code[i] ),

		.bk_load_sel( status[9] ),
		.bk_save_sel( status[10] ),
		.bk_osd_save( status[13] ),
		.osd_showing( OSD_STATUS ),

		.sav_supported( sav_supported[i] ),
		.sav_pending( sav_pending[i] ),

		// hps-related signals
		.ioctl_wr( ioctl_wr ),
		.ioctl_addr( ioctl_addr ),
		.ioctl_dout( ioctl_dout ),

		.sd_lba( sd_lba[i] ),
		.sd_rd( sd_rd[i] ),
		.sd_wr( sd_wr[i] ),
		.sd_ack( sd_ack ),
		.sd_buff_addr( sd_buff_addr ),
		.sd_buff_dout( sd_buff_dout ),
		.sd_buff_din( sd_buff_din[i] ),
		.sd_buff_wr( sd_buff_wr ),
		.img_mounted( img_mounted ),
		.img_readonly( img_readonly ),
		.img_size( img_size )
	);

	// the gameboy itself
	gb gb (
		.reset	    ( reset      ),

		.clk_sys     ( clk_sys    ),
		.ce          ( ce_cpu     ),   // the whole gameboy runs on 4mhnz
		.ce_2x       ( ce_cpu2x   ),   // ~8MHz in dualspeed mode (GBC)

		.fast_boot   ( status[2]  ),
		.joystick    ( joystick[i]),
		.isGBC       ( isGBC      ),
		.isGBC_game  ( isGBC_game[i] ),

		// interface to the "external" game cartridge
		.cart_addr   ( cart_addr[i] ),
		.cart_rd     ( cart_rd[i]   ),
		.cart_wr     ( cart_wr[i]   ),
		.cart_do     ( cart_do[i]   ),
		.cart_di     ( cart_di[i]   ),

		//gbc bios interface
		.gbc_bios_addr   ( bios_addr[i] ),
		.gbc_bios_do     ( bios_do[i]   ),

		// audio
		.audio_l 	 ( audio_l[i]  ),
		.audio_r 	 ( audio_r[i]  ),

		// interface to the lcd
		.lcd_clkena  ( lcd_clkena[i] ),
		.lcd_data    ( lcd_data[i]   ),
		.lcd_mode    ( lcd_mode[i]   ),
		.lcd_on      ( lcd_on[i]     ),
		.speed       ( speed[i]      ),

		// Palette download will disable cheats option (HPS doesn't distinguish downloads),
		// so clear the cheats and disable second option (chheats enable/disable)
		.gg_reset((code_download && ioctl_wr && !ioctl_addr) | cart_download | palette_download),
		.gg_en(~status[17]),
		.gg_code( gg_code[i] ),
		.gg_available( gg_available[i] ),

		// Gameboy link signals
`ifdef DUALCORE
		.serial_clk_out  ( serial_clk_out[i]),
		.serial_data_out ( serial_data_out[i]),
		.serial_clk_in   ( serial_clk_out[(i+1) % (NUM_CORES-1) ]),
		.serial_data_in  ( serial_data_out[(i+1) & (NUM_CORES-1)])
`else
		.serial_clk_out  (  ),
		.serial_data_out (  ),
		.serial_clk_in   (  ),
		.serial_data_in  (  )
`endif
	);

end
endgenerate

// the lcd to vga converter
wire [7:0] video_r, video_g, video_b;
wire video_hs, video_vs, video_bl;

lcd lcd (
	.pclk   ( clk_sys_old),
	.pce    ( ce_pix     ),
	.clk    ( clk_cpu    ),
	.isGBC  ( isGBC      ),

	.tint   ( status[1]  ),
	.inv    ( status[12]  ),

	// Palettes
	.pal1   (palette[127:104]),
	.pal2   (palette[103:80]),
	.pal3   (palette[79:56]),
	.pal4   (palette[55:32]),

	// serial interface
	.clkena1 ( lcd_clkena[0] ),
	.data1   ( lcd_data[0]   ),
	.mode1   ( lcd_mode[0]   ),  // used to detect begin of new lines and frames
	.on1     ( lcd_on[0]     ),

	.clkena2 ( lcd_clkena[NUM_CORES-1] ),
	.data2   ( lcd_data[NUM_CORES-1]   ),
	.mode2   ( lcd_mode[NUM_CORES-1]   ),  // used to detect begin of new lines and frames
	.on2     ( lcd_on[NUM_CORES-1]     ),

	.hs     ( video_hs   ),
	.vs     ( video_vs   ),
	.blank  ( video_bl   ),
	.r      ( video_r    ),
	.g      ( video_g    ),
	.b      ( video_b    )
);

assign VGA_SL = 0;
assign VGA_R  = video_r;
assign VGA_G  = video_g;
assign VGA_B  = video_b;
assign VGA_DE = ~video_bl;
assign CLK_VIDEO = clk_sys;
assign CE_PIXEL = ce_pix & !line_cnt;
assign VGA_HS = video_hs;
assign VGA_VS = video_vs;

// simple audio mixer for dual gb
wire [16:0] mixed_audio_l = audio_l[0] + audio_l[NUM_CORES-1];
wire [16:0] mixed_audio_r = audio_r[0] + audio_r[NUM_CORES-1];
wire [16:0] mixed_audio_c1 = audio_l[0] + audio_r[0];
wire [16:0] mixed_audio_c2 = audio_l[NUM_CORES-1] + audio_r[NUM_CORES-1];

assign AUDIO_L = status[17:16] == 0 ? audio_l[0] :
                 status[17:16] == 1 ? audio_l[NUM_CORES-1] :
					  status[17:16] == 2 ? mixed_audio_c1 :
					  mixed_audio_l;

assign AUDIO_R = status[17:16] == 0 ? audio_r[0] :
                 status[17:16] == 1 ? audio_r[NUM_CORES-1] :
					  status[17:16] == 2 ? mixed_audio_c2 :
					  mixed_audio_r;

wire clk_sys_old =  clk_sys & ce_sys;
wire ce_cpu2x = ce_pix;
wire clk_cpu = clk_sys & ce_cpu;
wire clk_cpu2x = clk_sys & ce_pix;

reg ce_pix, ce_cpu,ce_sys;
always @(negedge clk_sys) begin
	reg [3:0] div = 0;

	div <= div + 1'd1;
	ce_sys   <= !div[0];
	ce_pix   <= !div[2:0];
	ce_cpu   <= !div[3:0];
end

reg [1:0] line_cnt;
always @(posedge clk_sys_old) begin
	reg old_hs;
	reg old_vs;

	old_vs <= video_vs;
	old_hs <= video_hs;

	if(old_hs & ~video_hs) line_cnt <= line_cnt + 1'd1;
	if(old_vs & ~video_vs) line_cnt <= 0;
end

endmodule
