# Dual-core link feature for Gameboy for Mister

Note: the original README info is at the bottom of this readme

## Bradon Kanyid (rattboi) for CS561

This project is adding a new large feature to the Gameboy MiSTer core.

[MiSTer](https://github.com/MiSTer-devel/Main_MiSTer/wiki) is an open-source hybrid emulation ecosystem consisting of a traditional SoC (Arm dual-core cpu) and a mid-tier FPGA (Altera Cyclone V).
There is an existing Gameboy FPGA core for MiSTer. This project is to expand that FPGA core to support the following:

* 2 simultaneous cores running independently
* Each core is able to display their own display on a segment of a shared screen
* Multiplexing sound options (once out of sync, music from each core will conflict with each other)
* Link hardware emulation, in order for the two cores to interact (traditional link play)
* The ability to load a different game per-core, in order to support games like Pokemon Red/Blue, which work together

## Stretch goals
    - interaction between emulated core and physical hardware (original gameboy or peripheral) via external IO + modified link cable
    - TBD

This code is licensed [GPL3](LICENSE)

## Build Instructions

In order to build this code, you will need Intel's Quartus tools installed. The free Lite version is acceptable.

```
quartus_sh --flow compile Gameboy.qsf
```

## Installation instructions

Like any other MiSTer core, please copy the generated `Gameboy.rbf` file onto your SDcard and choose it from MiSTer's file picker. Alternatively, you can use the built-in USB JTAG blaster on the DE10-Nano hardware to flash directly to memory. This is faster for testing.

```
quartus_pgm -c 1 ./output_files/Gameboy.cdf
```

Below this is the original ReadMe.md of the project

# Gameboy for MiSTer

This is port of [Gameboy for MiST](https://github.com/mist-devel/mist-board/tree/master/cores/gameboy)

* Place RBF file into root of SD card.
* Place *.gb files into Gameboy folder.

## Open Source Bootstrap roms
This now includes the open source boot ROMs from [https://github.com/LIJI32/SameBoy/](https://github.com/LIJI32/SameBoy/) (for maximum GBC compatibility/authenticity you can still place the Gameboy color bios/bootrom into the Gameboy folder and rename it to boot1.rom)

## Palettes
Core supports custom palettes (*.gbp) which should be places into Gameboy folder. Some examples are available in palettes folder.

## Autoload
To autoload custom palette at startup rename it to boot0.rom
To autoload favorite game at startup rename it to boot2.rom

## Analog output 
Due to using a weird video resolution and frequencies (from a TV signal perspective) the core needs help from the scaler to output a 15KHz Signal.

For now you can append this to your MiSTer.ini configuration file (credit goes to ghogan42/soltan_g42) that enables the vga_scaler to be active when using this core

**be aware that you will lose HDMI output for this core** :

```ini
[Gameboy]
video_mode=320,8,32,24,240,4,3,16,6048
vga_scaler=1
vsync_adjust=2
vscale_mode=1
```


